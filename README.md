Client-Updater
==============

THIS IS STILL IN DEVELOPMENT 

Java program to update a Minecraft instance

Files to change for configuration to your system

ClientUpdater.java   CloudPath - This is the absolute path to the parent folder holding all mods as a string (C:\\Copy\\JamOORev\\ServerRelatedFiles\\[Main] #BlameTombenpotter\\mods)
                     InstancePath - This is the mods folder of your instance as a string (C:\\MultiMC\\\instances\\1.7.101\\minecraft\\mods)

Clientsides.txt - Contains the file names of the files you wish to keep that are not located in the folder pointed to by "CloudPath"

Report any issues here : https://github.com/StoneWaves/Client-Updater/issues

Provided by StoneWaves
