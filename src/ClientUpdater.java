import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;


public class ClientUpdater {

    public static void main(String[] args) throws IOException {
        //Cloud and Instance file paths
        String CloudPath = "C:\\Users\\Chris\\Dropbox\\MinecraftKris\\Old Files\\ServerRelatedFiles\\[Main] #BlameTombenpotter\\mods";
        String InstancePath = "C:\\Users\\Chris\\Documents\\Updater Test Folder";
        String LogPath = ".\\log\\";

        //Lists for comparisons
        ArrayList listA = new ArrayList( );
        ArrayList listB = new ArrayList( );
        ArrayList Approved = new ArrayList();
        ArrayList MarkedForRemoval = new ArrayList();
        ArrayList RemovalApproved = new ArrayList();


        //Generate list of files that are in each location
        LocationA(listA, CloudPath);
        LocationB(listB, InstancePath);

        //Generate a list of up-to-date file along with those that are not in the cloud
        InfoForA(listA, InstancePath, LogPath, MarkedForRemoval);
        //Generate list of new files in Location A
        InfoForB(listB, CloudPath, InstancePath, LogPath);

        FileRemoval(InstancePath, LogPath, Approved, MarkedForRemoval, RemovalApproved, LogPath);

        System.out.print("\nEnd");
    }

    private static void FileRemoval(String instancePath, String logPath, ArrayList approved, ArrayList markedForRemoval, ArrayList removalApproved, String LogPath) throws IOException {
        FileInputStream fis = new FileInputStream(logPath + "Clientsides.txt");

        //Construct BufferedReader from InputStreamReader
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String line = "";
        while ((line = br.readLine()) != null) approved.add(line);
        br.close();


        for (Object aMarkedForRemoval : markedForRemoval) {
            if (approved.contains(aMarkedForRemoval)) {
                //Do nothing
            } else removalApproved.add(aMarkedForRemoval);
        }

        String myFileName = LogPath + "Removed.txt";

        PrintWriter myOutFile;
        myOutFile =new PrintWriter(myFileName);

        for (Object aRemovalApproved : removalApproved) {
            Files.delete(Paths.get(instancePath + "\\" + aRemovalApproved));
            System.out.println();
            System.out.println("Removed : " + aRemovalApproved);
            myOutFile.print(aRemovalApproved);
        }



        myOutFile.close();
    }

    private static void InfoForB(ArrayList listB, String CloudPath, String InstancePath, String LogPath) throws IOException {
        String files;
        File folder = new File(CloudPath);
        File[] listOfFiles = folder.listFiles();
        String NameMissing = LogPath + "New Files.txt";
        PrintWriter OutMissing;
        OutMissing =new PrintWriter(NameMissing);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listB.contains(files)) {
                    //Do nothing
                } else {
                    OutMissing.print(files + "\n");
                    CopyOption[] options = new CopyOption[]{
                            StandardCopyOption.REPLACE_EXISTING,
                            StandardCopyOption.COPY_ATTRIBUTES
                    };

                    Path From = Paths.get(CloudPath + "\\" + files);
                    Path To = Paths.get(InstancePath + "\\" +  files);
                    Files.copy(From, To, options);
                    System.out.println();
                    System.out.println("Added : " + files);

                }
            }
        }
        OutMissing.close();

    }

    private static void InfoForA(ArrayList listA, String path, String LogPath, ArrayList MarkedForRemoval) throws IOException {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        String NameExists = LogPath + "Up to date.txt";
        PrintWriter OutExists;
        OutExists =new PrintWriter(NameExists);

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                if (listA.contains(files)) {
                    OutExists.print(files + "\n");
                } else {
                    MarkedForRemoval.add(files);
                }
            }
        }
        OutExists.close();
    }

    private static void LocationA(ArrayList listA, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                //System.out.println(files);
                listA.add(files);
            }
        }
        System.out.println(listA.size() + " file(s) in location cloud folder");
    }

    private static void LocationB(ArrayList listB, String path) {
        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles != null ? listOfFiles : new File[0]) {
            if (listOfFile.isFile()) {
                files = listOfFile.getName();
                listB.add(files);
            }
        }
        System.out.println(listB.size() + " file(s) in location instance folder");
    }
}